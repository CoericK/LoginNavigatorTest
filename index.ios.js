'use strict';
import React, {
    AppRegistry,
    Component,
    StyleSheet,
    Navigator,
    Text,
    View
} from 'react-native';

var Loading = require('./app/commons/loading');
var Login = require('./app/components/login');
var Main = require('./app/components/main');

class LoginNavigatorExample extends Component {
    renderScene(route, nav) {
        console.log(route, navigator);

        if (route.component) {
            return React.createElement(route.component, { navigator });
        }
        switch (route.id) {
            case 'login':
                return <Login navigator={nav}/>;
            case 'main':
                return <Main navigator={nav}/>;
            default:
                return (
                    <Loading
                        message={route.message}
                        navigator={nav}
                        />
                );
        }
    }

    render() {
        return (
            <Navigator
                initialRoute={{ message: "First Scene", }}
                renderScene={this.renderScene}
                configureScene={() => {
                    return Navigator.SceneConfigs.FloatFromRight;
                }}

                />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

AppRegistry.registerComponent('LoginNavigatorExample', () => LoginNavigatorExample);
