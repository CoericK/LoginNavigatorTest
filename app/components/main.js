'use strict';
import React, {
    AsyncStorage,
    AppRegistry,
    Component,
    StyleSheet,
    TouchableHighlight,
    Text,
    View
} from 'react-native';

var Profile = require('./profile');

class Main extends Component {
    logout() {
        AsyncStorage.removeItem('userID');
        this.props.navigator.replace({id: 'login'});
    }

    profile() {

        this.props.navigator.push({name: 'ProfileView', component: Profile});
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.instructions}>
                    Main View!
                </Text>

                <TouchableHighlight underlayColor='transparent' onPress={this.profile.bind(this)}>
                    <Text style={styles.instructions}>
                        Go to Profile
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight underlayColor='transparent' onPress={this.logout.bind(this)}>
                    <Text style={styles.instructions}>
                        Logout
                    </Text>
                </TouchableHighlight>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#03A9F4',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

module.exports = Main;