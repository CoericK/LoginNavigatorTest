'use strict';
import React, {
    AsyncStorage,
    AppRegistry,
    Component,
    StyleSheet,
    TouchableHighlight,
    Text,
    View
} from 'react-native';

class Profile extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.instructions}>
                    Profile View!
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0091EA',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

module.exports = Profile;