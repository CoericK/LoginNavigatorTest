'use strict';
import React, {
    AppRegistry,
    AsyncStorage,
    Component,
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from 'react-native';

class Login extends Component {

    signIn() {
        AsyncStorage.setItem("userID", "1");
        this.props.navigator.replace({id:'main'});

    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.instructions}>
                    Login View!
                </Text>

                <TouchableHighlight underlayColor='transparent' onPress={this.signIn.bind(this)}>
                    <Text style={styles.instructions} >
                        Sign In
                    </Text>
                </TouchableHighlight>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00BCD4',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

module.exports = Login;