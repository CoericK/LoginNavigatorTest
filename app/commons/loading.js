'use strict';
import React, {
    AppRegistry,
    Component,
    StyleSheet,
    AsyncStorage,
    TouchableHighlight,
    Text,
    View
} from 'react-native';

class Loading extends Component {


    componentDidMount() {
        AsyncStorage.getItem('userID').then((userID) => {
            this.setState({
                logged: userID !== null
            });
            if (this.state.logged) {
                this.props.navigator.replace({id: 'main'});
            } else {
                this.props.navigator.replace({id: 'login'});
            }

        });

    }


    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.instructions}>
                    Loading...
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

module.exports = Loading;